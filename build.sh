#!/bin/sh

CONFIG=settings.conf
BUILD=build

missing_jre_version() {
 cat << EOF
Java version not defined. Please specify the JAVA_VERSION environment variable
Must be one of the tags from the official Java repository @ dockerhub. See http://bit.ly/1LbApPG
Examples: openjdk-7u95-jre, 7-jre, openjdk-8
EOF
 exit 1
}

missing_newrelic_version() {
 cat << EOF
NewRelic version not defined. Please specify the NEWRELIC_VERSION envoironment variable
Current version can be found in NewRelic's documentation: http://bit.ly/1QAVkui
Example: 3.26.1
EOF
 exit 2
}

do_build() {
 [ -z "$JAVA_VERSION" ] && missing_jre_version
 [ -z "$NEWRELIC_VERSION" ] && missing_newrelic_version

 mkdir -p $BUILD

 echo "FROM openjdk:$JAVA_VERSION

 ENV NEWRELIC_VERSION $NEWRELIC_VERSION

 ENV JAVA_OPTS='-javaagent:/lib/newrelic.jar -Dnewrelic.config.file=/etc/newrelic/newrelic.yml'
 ADD /etc/newrelic.yml /etc/newrelic/
 ADD http://download.newrelic.com/newrelic/java-agent/newrelic-agent/${NEWRELIC_VERSION}/newrelic-agent-${NEWRELIC_VERSION}.jar /lib/newrelic.jar" > $BUILD/Dockerfile

 cp -R src/* $BUILD
}

[ -r "$CONFIG" ] && source $CONFIG

case $1 in
	build)
		do_build
		;;
	clean)
		rm -rf $BUILD
		;;
	*)
		echo "Usage: $0 <build|clean>"
		exit 3
esac
